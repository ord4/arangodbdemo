from flask import render_template, flash, redirect
from app import app
from app.forms import LoginForm, SignUpForm, RatingForm
from app.session import Session
from pyArango.connection import *
from werkzeug.security import generate_password_hash, check_password_hash
#from pyArango.theExpceptions import DocumentNotFoundError

db = Connection()["MovieRater"]
user_collection = db["Users"]
session = Session("")

@app.route('/')
@app.route('/index', methods=['GET', 'POST'])
def index():
    session.end()
    
    form = LoginForm()
    if form.validate_on_submit():
        session.set_user_key('_'+form.username.data)

        try:
            # Validate the credentials
            #if (form.password.data ==  user_collection['_'+form.username.data]["password"]):
            if (check_password_hash(user_collection[session.get_user()]["password"], form.password.data)):
                ratings = user_collection['_'+form.username.data]["ratings"].getStore()
                return redirect('/home') 
            else:
                flash("Invalid login. Try again.")
        # Currently catches any exception that is thrown, trying to catch specifically the DocumentNotFoundError
        # Occurs when a user enters a login that does not exist in the database
        except:
            flash("Looks like that user does not exist! Try logging in with a valid account, or create one!")
    return render_template('index.html', title='Log In', form=form)

@app.route('/home', methods=['GET', 'POST'])
def home():
    if (session.get_user() == ""):
        flash("You must log in before you can rate movies!")
        return redirect("/index")
    else:
        form = RatingForm()

        ratings = user_collection[session.get_user()]["ratings"].getStore()
    
        if form.validate_on_submit():
            doc = user_collection[session.get_user()]
            doc["ratings"]["star-wars-the-force-awakens"] = form.star_wars_rating.data
            doc["ratings"]["the-hobbit-an-unexpected-journey"] = form.hobbit_rating.data
            doc["ratings"]["monty-python-and-the-holy-grail"] = form.monty_python_rating.data
            doc["ratings"]["harry-potter-and-the-deathly-hallows-pt-2"] = form.harry_potter_rating.data
            doc["ratings"]["a-quiet-place"] = form.quiet_place_rating.data
            doc.save()
            
            ratings = user_collection[session.get_user()]["ratings"].getStore()
            
            return render_template('home.html', title='Home', form=form, ratings=ratings)


        return render_template('home.html', title='Home', form=form, ratings=ratings)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignUpForm()

    if form.validate_on_submit():
        # Check the passwords match
        if form.password.data != form.conf_pswd.data:
            flash("Passwords do not match.")
            return render_template('signup.html', title='Sign Up', form=form)
        
        # Validate that there is no current user with that name/key
        for doc in user_collection.fetchAll():
            if doc["_key"] == '_' + form.username.data:
                flash("An account with that username already exists. Please try again with a different username.")
                return render_template('signup.html', title='Sign Up', form=form)

        # If we fall through we have a unique user
        # Construct new user document
        usr_doc = user_collection.createDocument()
                
        usr_doc["username"] = form.username.data

        password = generate_password_hash(form.password.data)
        usr_doc["password"] = password
                
        usr_doc["_key"] = "_" + form.username.data

        # Insert unraked values for movis
        usr_doc["ratings"] = {
            "star-wars-the-force-awakens": "",
            "the-hobbit-an-unexpected-journey": "",
            "monty-python-and-the-holy-grail": "",
            "harry-potter-and-the-deathly-hallows-pt-2": "",
            "a-quiet-place": ""
            }

        usr_doc.save() # Write user to the database

        session.set_user_key(usr_doc["_key"])
        
        # Need to pass the loaded user ratings
        return redirect('/home')

        # Redirect them to home (to begin rating movies)
    
    return render_template('signup.html', title='Sign Up', form=form)
