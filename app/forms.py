from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
    username = StringField('Username', render_kw={"placeholder":'Username'}, validators=[DataRequired()])
    password = PasswordField('Password', render_kw={"placeholder":'Password'}, validators=[DataRequired()])
    submit = SubmitField('Log in')

class SignUpForm(FlaskForm):
    username = StringField('Username', render_kw={"placeholder":'Username'}, validators=[DataRequired()])
    password = PasswordField('Password', render_kw={"placeholder":'Password'}, validators=[DataRequired()])
    conf_pswd = PasswordField('Confirm password', render_kw={"placeholder":'Confirm Password'}, validators=[DataRequired()])
    submit = SubmitField('Sign Up')

class RatingForm(FlaskForm):
    star_wars_rating = StringField('Star Wars: The Force Awakens')
    hobbit_rating = StringField('The Hobbit: An Unexpected Journey')
    monty_python_rating = StringField('Monty Python and the Holy Grail')
    harry_potter_rating = StringField('Harry Potter and the Deathly Hallows Part 2')
    quiet_place_rating = StringField('A Quiet Place')
    submit = SubmitField('Update ratings')                             
