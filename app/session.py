class Session(object):
    def __init__(self, key):
        self.user = key
    
    def set_user_key(self, key):
        self.user = key

    def get_user(self):
        return self.user

    def end(self):
        self.user = ""

