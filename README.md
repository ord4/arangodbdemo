# MovieRater
This is a simple web application to demonstrate an example program
using [ArangoDB](https://arangodb.com). This web application was built
using Flask. A [tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world) from Miguel Grinberg was followed and adapted
for this application.

### TODO
- [X] Add authentication logic to login form
  - [X] Connect to DB within app
- [ ] Create a display class for different movies and ratings
- [ ] Style application
- [ ] Catch exception if there is no user matching the entered credentials
- [X] Hash passwords before storing
- [X] Add a way to logout/end the session
- [X] Update the ratings and write the changes to the database 
- [ ] Validate that the movie ratings are within range and a number

### Getting Started
To begin development for this application first start by creating a
Python virtual environment

```bash
python3 -m venv env
```

The above command will create a virtual environment of the name
"env". Now that you have your environment set up you can activate your
environment and then install the required pip packages.

```bash
source env/bin/activate env
pip3 install -r requirements.txt
```

Make sure in the the base directory there is a _config.py_ file holding
the Config class which should have information about the secret
key. You can now develop the application as you please.

### Running the App
To run the app simply use the following commands:
```bash
export FLASK_APP=app.py
flask run
```

### ArangoDB
Currently I am using ArangoDB from a docker container so I could get
up and running with the database. It may be worth figuring out how to
start it up on my own machine without Docker so I can make sure it is
customized to fit my needs. I haven't tried connecting to the DB yet,
so there may be some issues in running through Docker.

To start up ArangoDB with persistent storage using a volume use the
command:
```bash
sudo docker run -e ARANGO_NO_AUTH=1 -p 8529:8529 -v local-dir:/var/lib/arangodb3 arangodb/arangodb
```
This exposes the container to localhost at the port 8529 with no
authentication necessary (this should be revisted when customizing the
system).
